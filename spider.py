from pyquery import PyQuery as pq
import urllib
import threading

TARGET_URL = 'https://news.bg/bulgaria'
NEWS_LIST_ITEM_ELEMENT = 'ul.secondary-articles > li'
NEWS_LIST_TITLE_ELEMENT = 'h2'
NEWS_LIST_LINK_ELEMENT = 'h2 > a'
NEWS_ARTICLE_TEXT_ELEMENT = 'div.article-text'

news_titles = {}
news_urls = {}


def main():
    doc = fetch_url_data()
    news_list = get_news_list(doc)
    news_list.map(lambda i, li: fetch_news_data(i, pq(li)))

    threads = []
    for i in news_urls:
        t = threading.Thread(target=fetch_news_url_data, args=(i, news_urls[i]))
        threads.append(t)
        t.start()

    for t in threads:
        t.join()

    # TODO: proccess fetched data
    print("TODO: proccess data...")


def fetch_url_data():
    with urllib.request.urlopen(TARGET_URL) as response:
        return pq(response.read())


def fetch_news_url_data(i: int, url: str):
    with urllib.request.urlopen(url) as res:
        article = pq(res.read())(NEWS_ARTICLE_TEXT_ELEMENT).text()
        # TODO: save data in variables
        print(news_titles[i], '\n')
        print(article)
        print('\n\n\n')


def get_news_list(doc: pq):
    news_list = doc(NEWS_LIST_ITEM_ELEMENT)
    return news_list


def fetch_news_data(i: int, el: pq):
    news_titles[i] = fetch_news_title(el)
    news_urls[i] = fetch_news_url(el)


def fetch_news_title(el: pq):
    return el(NEWS_LIST_TITLE_ELEMENT).text()


def fetch_news_url(el: pq):
    return el(NEWS_LIST_LINK_ELEMENT).attr('href')


if __name__ == '__main__':
    main()
